package com.fdn.anything_warm

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.fdn.anything_warm.rest.TemperatureService
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import java.math.BigDecimal
import java.util.*
import kotlin.concurrent.schedule

val retrofit = Retrofit.Builder()
    .baseUrl("http://5.8.37.129:8080/") //Базовая часть адреса
    .addConverterFactory(GsonConverterFactory.create()) //Конвертер, необходимый для преобразования JSON'а в объекты
    .build()

val service = retrofit.create(TemperatureService::class.java)

lateinit var newGoalValue: EditText
lateinit var saveGoal: Button
lateinit var goalValue: TextView
lateinit var currentTemperatureValue: TextView
lateinit var info: TextView

val goalWait = MainActivity.GoalWait()
val saveGoalListener = MainActivity.SaveGoalListener()
val tempWait = MainActivity.TempWait()

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        newGoalValue = findViewById(R.id.newGoalTemperature)
        saveGoal = findViewById(R.id.saveGoalTemperature)
        goalValue = findViewById(R.id.goalTemperature)
        currentTemperatureValue = findViewById(R.id.currentTemperature)
        info = findViewById(R.id.info)

        saveGoal.setOnClickListener(saveGoalListener)

        service.getGoal().enqueue(goalWait)
        service.getTemperature().enqueue(tempWait)

        Timer().schedule(object : TimerTask() {
            @SuppressLint("SetTextI18n")
            override fun run() {
                service.getTemperature().enqueue(tempWait)
            }
        }, 1, 15000)
    }

    class SaveGoalListener : View.OnClickListener {
        @SuppressLint("SetTextI18n")
        override fun onClick(v: View?) {
            info.text = ""

            val goal = newGoalValue.text.toString().toBigDecimal()
            if (goal < BigDecimal(15) || goal > BigDecimal(30)) {
                info.text = "Goal must be greater than 15 and less than 30"
            } else {
                service.saveGoal(goal).enqueue(goalWait)
            }
        }
    }

    class GoalWait : Callback<BigDecimal> {
        override fun onFailure(call: Call<BigDecimal>, t: Throwable) {}

        override fun onResponse(call: Call<BigDecimal>, response: Response<BigDecimal>) {
            goalValue.text = response.body().toString()
        }
    }

    class TempWait : Callback<BigDecimal> {
        @SuppressLint("SetTextI18n")
        override fun onFailure(call: Call<BigDecimal>, t: Throwable) {
            currentTemperatureValue.text = "Server not available"
        }

        override fun onResponse(call: Call<BigDecimal>, response: Response<BigDecimal>) {
            currentTemperatureValue.text = response.body().toString()
        }
    }
}

