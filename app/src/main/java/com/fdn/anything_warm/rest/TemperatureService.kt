package com.fdn.anything_warm.rest

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import java.math.BigDecimal


interface TemperatureService {
    @GET("/temperature")
    fun getTemperature(): Call<BigDecimal>
    @GET("/goal")
    fun getGoal(): Call<BigDecimal>
    @POST("/goal")
    fun saveGoal(@Body goal: BigDecimal): Call<BigDecimal>
}